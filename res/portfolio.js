$(window).load(function(){
	$(".loading").css({display:"none"});
	$(".left-display-area").addClass("noshadow");
	$(".left-display-area .direction-image a").click(function(){
		$(".left-display-area").css({
			left: "99.5%"
		}).addClass("noshadow");
	});
	$(".item .hover-image").click(function(){
		//get html
		var container = $(this).parent().children(".body");
		var heading = container.children(".heading").html();
		var image = container.children(".image").html();
		if (image.indexOf("carousel-") > -1 ){
			var carousel = container.children(".image").children(".carousel").children(".carousel-inner").html();
			console.log(carousel);
		}
		else{
			var carousel = 0;
		}
		var textContainer =  container.children(".text");
		var explanation = textContainer.children(".explanation").html();;
		var time = textContainer.children(".time").html();;
		var challenges = textContainer.children(".challenges").html();;
		var conclude = textContainer.children(".conclude").html();
		var skills = textContainer.children(".skills").html();
		
		
		//set text
		// console.log($(".left-display-area").children(".heading").html());
		$(".left-display-area .heading").html(heading);
		if (carousel){
			$(".left-display-area #carousel-left").removeClass("hidden");
			$(".left-display-area .image .inner-image").addClass("hidden");
			$(".left-display-area #carousel-left .carousel-inner").html(carousel);
		}
		else{
			$(".left-display-area #carousel-left").addClass("hidden");
			$(".left-display-area .image .inner-image").removeClass("hidden");
			$(".left-display-area .image .inner-image").html(image);
		}
		$(".left-display-area .explanation").html(explanation);
		$(".left-display-area .time .content").html(time);
		$(".left-display-area .challenges .content").html(challenges);
		$(".left-display-area .conclude .content").html(conclude);
		$(".left-display-area .skills .content").html(skills);

		console.log( $('.left-display-area #carousel-left').carousel() );

		$(".left-display-area").css({
			left: "10%"
		}).removeClass("noshadow");
		$(".left-display-area .explanation").focus();

	
	});
});