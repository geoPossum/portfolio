import os, json, requests, webbrowser,pickle, urlparse

DIR = os.path.dirname(__file__)
SETTINGS = os.path.join(DIR,"settings.xyz")
OUTPUT = os.path.join(DIR,"OUTPUT")
if not os.path.exists( OUTPUT ):
    os.mkdir( OUTPUT )
assert os.path.exists( OUTPUT ), "Could not create output directory"

##CLIENT_ID =
##REDIRECT_URI =
TYPE = "token"
access_token = ""
auth_url = "https://api.instagram.com/oauth/authorize/\
?client_id={0}\
&redirect_uri={1}\
&response_type={2}".format(CLIENT_ID, REDIRECT_URI, TYPE)

if os.path.exists(SETTINGS):
    with open(SETTINGS) as w:
        access_token = pickle.load(w)

if not access_token:
    webbrowser.open(auth_url)

    access_token = raw_input("Please Copy Access Token Here:").strip(" ").lstrip("code=")
    print "Access Token is", access_token
    
    with open(SETTINGS,"a") as w:
        pickle.dump(access_token, w)


print "Using access token", access_token

INSTAGRAM_API = "https://api.instagram.com/v1"
ENDPOINTS = {
    "info":"/users/self",
    "my_feed":"/users/self/feed"
    }
print "Getting feed"
feed = requests.get(INSTAGRAM_API+ENDPOINTS["my_feed"], params={
    "access_token":access_token,
    "count":5
    })
if feed.ok:
    data = json.loads(feed.content)
    print "+++++++++++++++Your data++++++++++++++ "
    print data

