import sys
debug = 1

def checkcolumnitem(column, number):
    """Check if this column has the number"""
    for row in sudoku:
        if row[column] == number:
            return True
    return False

def canbebox(row, column, number):
    """Returns true if the number does not have a duplicate in the box"""
    #the quotient indicates whether item is in box 0, 1 or 2
    rowquotient = row/3
    colquotient = column/3

    for j in range(0,3):    #row
        row_ = rowquotient*3+j
        for k in range(0,3):    #column
            col = colquotient*3+k
            if (sudoku[row_][col]==number) and (col!=column) :
                return False
    return True

def canberow(row, column, number):
    """Returns true if the number does not have a duplicate in the row"""
    for index, entry in enumerate(sudoku[row]):
        if index!=column:
            if entry==number:
                return False
    return True

def canbecolumn(row, column, number):
    """Returns true if the number does not have a duplicate in the row"""
    for row in sudoku:
        if row[column]==number:
            return False
    return True

def canbehorizontalboxes(row, column, number):
    """Check if the same number can be found in different rows in boxes horizontally adjacent to this"""
    #change the columns to reference the next boxes
    column1 = (column+3)%9
    column2 = (column+6)%9
    if boxhasnumber(row,column1,number) and boxhasnumber(row,column2,number):
        return True
    return False

def canbeverticalboxes(row, column, number):
    """Check if the same number can be found in different rows in boxes vertically adjacent to this"""
    #change the columns to reference the next boxes
    row1 = (row+3)%9
    row2 = (row+6)%9
    if boxhasnumber(row1,column,number) and boxhasnumber(row2,column,number):
        return True
    return False

def getemptyvaluesinbox(row,column):
    l = []
    for i in range(0,9):
        if not boxhasnumber(row,column,number):
            l.append(i)
    return l
    
def boxhasnumber(row,column,number):
    """Returns True if a box has a number"""
    #the quotient indicates whether item is in box 0, 1 or 2
    rowquotient = row/3
    colquotient = column/3
    for j in range(0,3):    #row
        row_ = rowquotient*3+j
        for k in range(0,3):    #column
            col = colquotient*3+k
            if (sudoku[row_][col]==number):
                return True
    return False

def printsudoku(emptyvalue="-"):
    """Print puzzle in a graphically pleasing manner. Empty value shall be used to replace 0's in the puzzle."""
    for rowindex, row in enumerate(sudoku):
        if rowindex%3 == 0:
            print 20*"--"
        for itemindex, item in enumerate(row):
            if item == 0:
                item = emptyvalue
            print item," ",
            if itemindex%3==2:
                print "|",
        print ""

def getvalidboxes(row,column,number):
    """Return a list of valid locations in a box that the number can enter in format
    [rowoffset, columnoffset,rowabs,colabs] where offsets are calculated from the box top-left."""
    boxrows = [0,1,2]
    boxcolumns = [0,1,2]
    for r in range(0,3):
        #e.g. r8 = 8 - 2 +r = 5 + r
        if not canberow( row-(row%3)+r, column, number):
            boxrows[r] = '-'
    for c in range(0,3):
        if not canbecolumn( row, column-(column%3)+c, number):
            boxcolumns[c] = '-'
            
##    print boxrows, boxcolumns
    
    l = []
    for i in range(0,3):
        if boxrows[i]!='-' :
            for j in range(0,3):
                if boxcolumns[j]!='-':
                    if sudoku[row-(row%3)+i][column-(column%3)+j]==0:
                        l.append([i,j,row-(row%3)+i,column-(column%3)+j])
    return l

def getmissingnumbers(row,column):
    """given topright corner coords,returns a list of values not in the box 3x3"""
    l = [i for i in range(1,10)]
    for r in range(0,3):
        for c in range(0,3):
            if sudoku[row+r][column+c]!=0:
                del l[l.index(sudoku[row+r][column+c])]
    return l

easysudoku = [
    [0, 0, 8, 0, 3, 0, 5, 4, 0],
    [3, 0, 0, 4, 0, 7, 9, 0 ,0],
    [4, 1, 0, 0, 0, 8, 0, 0, 2],
    [0, 4, 3, 5, 0, 2, 0, 6, 0],
    [5, 0, 0, 0, 0, 0, 0, 0, 8],
    [0, 6, 0, 3, 0, 9, 4, 1, 0],
    [1, 0, 0, 8, 0, 0, 0, 2, 7],
    [0, 0, 5, 6, 0, 3, 0, 0, 4],
    [0, 2, 9, 0, 7, 0, 8, 0, 0]
    ]
#not solved yet
hardsudoku = [
    [0, 6, 0, 0, 0, 7, 5, 2, 0],
    [0, 0, 0, 9, 0, 0, 6, 0 ,0],
    [1, 0, 0, 0, 0, 0, 8, 0, 7],
    [0, 0, 0, 3, 0, 0, 0, 1, 2],
    [0, 0, 3, 5, 0, 1, 4, 0, 0],
    [4, 2, 0, 0, 0, 8, 0, 0, 0],
    [5, 0, 8, 0, 0, 0, 0, 0, 6],
    [0, 0, 7, 0, 0, 9, 0, 0, 0],
    [0, 9, 6, 4, 0, 0, 0, 8, 0]
    ]
#waiting
sudoku=[
    [0, 0, 0, 0, 7, 4, 3, 1, 6],
    [0, 0, 0, 6, 0, 3, 8, 4, 0],
    [0, 0, 0, 0, 0, 8, 5, 0, 0],
    [7, 2, 5, 8, 0, 0, 0, 3, 4],
    [0, 0, 0, 0, 3, 0, 0, 5, 0],
    [0, 0, 0, 0, 0, 2, 7, 9, 8],
    [0, 0, 8, 9, 4, 0, 0, 0, 0],
    [0, 4, 0, 0, 8, 5, 9, 0, 0],
    [9, 7, 1, 3, 2, 6, 4, 8, 5],
    
    ]

#check that we have good rows.
for row in sudoku:
    assert len(row)==9, 'All rows not 9. this is 9x9.'

##print "Puzzle is:"
##printsudoku('0')
##sys.exit(0)


#go to a box, first traverse horizontally, then vertically
#get all values not in boxes
#for each value, check which boxes are cancelled out by horizontal and vertical boxes
#when only one box is left, fill that box
#

unsolved = 1
ver = 0
foundmissing=0
while unsolved:
    hor = 0
    while hor<3:
        print 10*'-','BOX',ver,hor
        missing = getmissingnumbers(ver*3, hor*3)
        if len(missing):
            foundmissing = 1
        for number in missing:
            valid = getvalidboxes(ver*3, hor*3, number)
            print number, valid
            if len(valid) == 1:
                print 'ASSIGNED number',number,'to pos',valid[0][2],valid[0][3]
                sudoku[valid[0][2]][valid[0][3]] = number
        hor+=1
    ver+=1
    if ver/3:
        #we have reached end of 
        if not foundmissing:
            unsolved = 0
        #reset this
        foundmissing = 0
        
    ver%=3
            
##printsudoku()
