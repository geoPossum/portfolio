import os,json, requests, pickle, re

DIR = os.path.dirname(__file__)

class ExchangeRateLab:
    def __init__(self):
        self.settings_file = os.path.join(DIR,"ExchangeRateLab.xyz")
        #self.api_key = ""
        self.currency_codes_api = "http://api.exchangeratelab.com/api/currencies?apikey={0}".format(self.api_key)
        self.top_currencies_api = "http://api.exchangeratelab.com/api/current?apikey={0}".format(self.api_key)
        self.single_usd_api = "http://api.exchangeratelab.com/api/single/-->base-->?apikey={0}".format(self.api_key)
        if not os.path.exists(self.settings_file):
            self.settings = {}
        else:
            with open(self.settings_file) as w:
                try:
                    self.settings = pickle.load(w)
                except Exception as e:
                    self.settings = {}
                    self.save()
        
    def get_top_currencies(self):
        if self.settings.has_key("top_currencies"):
            return self.settings["top_currencies"]
        
        response = requests.get( self.top_currencies_api )
        if response.ok:
            content = json.loads(response.content)
            print content
            self.settings["top_currencies"] = content["rates"]
            self.save()
            return content["rates"]
        else:
            raise Exception("Could not get top currentcy list for some reason")
    
    def get_currency_codes(self):
        if self.settings.has_key("currency_codes"):
            return self.settings["currency_codes"]
        
        response = requests.get(self.currency_codes_api)
        if response.ok:
            content = json.loads(response.content)
            self.settings["currency_codes"] = content["currencies"]
            self.save()
            return content["currencies"]
        else:
            raise Exception("Could not get currentcy List for some reason")

    def get_currency_code(self,country):
        codes = self.get_currency_codes()
        for entry in codes:
            if country.lower().strip(" ") in entry["currencyDescription"].lower():
                return entry["currencyCode"]
            
    def get_single_usd(self,currency):
        if self.settings.has_key("single_usd"):
            if self.settings["single_usd"].has_key(currency):
                return self.settings["single_usd"][currency] 
        
        response = requests.get(self.single_usd_api.replace("-->base-->",currency))
        if response.ok:
            content = json.loads(response.content)
            if not self.settings.has_key("single_usd"):
                self.settings["single_usd"] = {}
            self.settings["single_usd"][currency] = content["rate"]["rate"]
            self.save()
            return float(content["rate"]["rate"])
        else:
            raise Exception("Could not get currentcy List for some reason")
        
    
    def save(self):
        with open(self.settings_file, 'w') as w:
                pickle.dump(self.settings, w)
        

erl = ExchangeRateLab()
print erl.get_single_usd("KSH")
print erl.get_top_currencies()
